myApp.directive('customButton', function () {
	return {
		restrict: 'A', 
		replace: true, 
		transclude: true,
		template: '<a href="#{{user.details.username}}">Minha Diretiva</a>',
		link: function (scope, element, attrs) {
			// Manipulação e Eventos DOM aqui!
		}
	};
});
