myApp.controller('UserCtrl', ['$scope', function ($scope) {
	//Criando o namespace user details
	//Que nos ajudará no visual do DOM
	$scope.user = {};
	$scope.user.details = { 
			"username": "Matioli",
			"id": "862034",
			"nome": "J. A. Matioli"
		};

	 $scope.user.disciplinas = [
	 	{id:'1', nome:"Desenv. App. WEB", faltas:'3', media:'7.5'},
	 	{id:'3', nome:"Desenv. Disp. Móveis", faltas:'2', media:'6.0'},
	 	{id:'11', nome:"Estruturas de dados", faltas:'5', media:'6.5'}
	 ]
	}
]);
