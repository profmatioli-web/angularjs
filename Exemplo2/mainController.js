var myApp = angular.module('myApp', []);

myApp.controller('MainCtrl', ['$scope', function ($scope) {
	// Controlador mágico
		$scope.hello = "Olá, sejam bem vindo ao AngularJs!";
	}
]);

myApp.controller('UserCtrl', ['$scope', function ($scope) {
	//Criando o namespace user details
	//Que nos ajudará no visual do DOM
	$scope.user = {};
	$scope.user.details = { 
			"username": "Matioli",
			"id": "862034"
		};
	}
]);

