var myApp = angular.module('myApp', []);

myApp.controller('MainCtrl', ['$scope', '$http', function($scope, $http) {
    $scope.idUsuario = '';
  $scope.usuario = {};
  var config = {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
    }
  }
  $scope.getUser = function(){
      var url = 'http://143.106.241.1/matioli/daw2017/api/v1/usuario/'+$scope.idUsuario;
      $http.get(url,config)
          .then(
              function(response){
                  var resposta = response['data'];
                  $scope.usuario.nome = resposta['data']['nome'];
                  $scope.usuario.email = resposta['data']['email'];
              },
              function(response){
                  $scope.usuario.nome = response['data']['status'];
              }
          );
  }

  // var url = 'http://143.106.241.1/matioli/daw2017/api/v1/usuario/1';
  // $http.get(url,config)
  // .then(
  //        function(response){
  //            var resposta = response['data'];
  //            $scope.usuario.nome = resposta['data']['nome'];
  //            $scope.usuario.email = resposta['data']['email'];
  //        },
  //        function(response){
  //            $scope.usuario.nome = response['data']['status'];
  //        }
  //     );
}]);

