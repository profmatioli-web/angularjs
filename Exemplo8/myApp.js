var myApp = angular.module('myApp', ["ngRoute"]);
myApp.config(function($routeProvider)
{
  /**
   * $routeProvider
   */
   $routeProvider
   .when("/", {
       templateUrl : "views/main.html"
   })
   .when("/red", {
       templateUrl : "views/red.html"
   })
   .when("/green", {
       templateUrl : "views/green.html"
   })
   .when("/blue", {
       templateUrl : "views/blue.html"
   })
   .otherwise({
     redirectTo:'views/others.html'
   });
});
