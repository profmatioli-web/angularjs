var myApp = angular.module('myApp', []);
myApp.controller("DivisionController",['$scope',function($scope){
    $scope.data = {number: 0, divisor: 0, result: 0};
    $scope.divisionNeeded = function () {
        $scope.data.result = $scope.data.number / $scope.data.divisor;
    }
}]);